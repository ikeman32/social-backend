require("dotenv").config();
const express = require("express"); 
const helmet = require("helmet");
const cors = require("cors");
const morgan = require(`morgan`);

//Middleware
const authenticate = require("./authenticate-middleware.js")

//Routers
const auth_router = require("../auth/auth_router.js");
const user_router = require(authenticate, "../users/user_router.js");

const server = express();

server.use(helmet());
server.use(morgan("dev"));
server.use(express.json());
server.use(cors());

//Routes
server.use("/api/auth", auth_router);
server.use("/api/user", authenticate, user_router);

server.get("/", (req, res) => {
  res.status(200).json({ api: "up", dbenv: process.env.DB_ENV });
});

module.exports = server;
