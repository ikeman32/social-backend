const db = require("../database/dbConfig");

const tblAuth = "[dbName]";//define the users table name

module.exports = {
  add,
  find,
  findBy,
  findById,
};

function find() {
  return db(tblAut).select("id", "username");
}

function findBy(filter) {
  return db(tblAuth).where(filter);
}

async function add(user) {
  const [id] = await db(tblAuth).insert(user);

  return findById(id);
}

function findById(id) {
  return db(tblAuth)
    .where({ id })
    .first();
}
